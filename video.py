import time

import matplotlib.pyplot as plt
from stegano import lsb
from encryption import AES
from time import process_time
import cv2
import logging

logging.basicConfig(level=logging.INFO)
import math
import os
import shutil
import numpy as np
import random
from tqdm import tqdm
import matplotlib.pyplot as plt
from subprocess import call, STDOUT


class VideoProcessing:

    def __init__(self):
        self.e = AES.AESEncrypt()
        self.inputTextDir = "./input/text/test/"
        self.outputTextDir = "./output/text/test/"
        self.inputVideoDir = "./input/video/test/"
        self.outputVideoDir = "./output/video/test/"

    def write_file(self, data, file_name):
        with open(file_name, 'w') as f:
            f.write(data)

    def split_string(self, s_str, count=400):
        per_c = math.ceil(len(s_str) / (count))
        if per_c % 8 != 0:
            per_c = per_c + (8 - per_c % 8)
        c_cout = 0
        out_str = ''
        split_list = []
        for i in range(len(s_str)):
            out_str += s_str[i]
            c_cout += 1
            if c_cout == per_c:
                split_list.append(out_str)
                out_str = ''
                c_cout = 0

        if c_cout != 0:
            leftOver = 8 - c_cout % 8
            for c in range(leftOver):
                out_str += " "
            split_list.append(out_str)
        return split_list

    def checkLength(self, s_str, count):
        while True:
            spliced_data = self.split_string(s_str, count)

            if self.count >= len(spliced_data):
                break
            count -= 1

    def shi_tomashi(self, image):
        gray_img = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        corners = cv2.goodFeaturesToTrack(gray_img, 1000, 0.0001, 15)
        corners = np.int0(corners)
        for i in corners:
            x, y = i.ravel()
            cv2.circle(image, (x, y), 3, (255, 0, 0), -1)
        return image

    def frame_extraction(self, video, shi_tomashi=False):
        if not os.path.exists("./tmp"):
            os.makedirs("tmp")
        temp_folder = "./tmp"
        logging.info("[INFO] tmp directory is created")

        vidcap = cv2.VideoCapture(video)
        count = 0

        while True:
            success, image = vidcap.read()
            if not success:
                break
            if shi_tomashi:
                image = self.shi_tomashi(image)
            cv2.imwrite(os.path.join(temp_folder, "{:d}.png".format(count)), image)

            count += 1
        self.count = count

    def encode_string(self, input_string, root="./tmp/", shi=False):
        global e
        split_string_list = self.split_string(s_str=str(input_string), count=self.count)
        count = len(split_string_list)
        start_encode = process_time()
        if len(os.listdir(root)) < count:
            full = False
            data_wasted = (count - len(os.listdir(root))) / len(os.listdir(root))
        else:
            full = True
            data_wasted = 0
        for i in tqdm(range(0, count)):
            f_name = "{}{}.png".format(root, i)
            secret_enc = lsb.hide(f_name, split_string_list[i])
            secret_enc.save(f_name)
        end_encode = process_time()
        return end_encode - start_encode, count, full, data_wasted

    def decode_string(self, video, length, shi=False):
        global e
        i = 1
        self.frame_extraction(video, True)
        secret = []
        root = "./tmp/"
        start_d = process_time()
        for i in tqdm(range(length)):
            f_name = "{}{}.png".format(root, i)
            i += 1
            secret_dec = lsb.reveal(f_name)
            if secret_dec == None:
                break
            secret.append(secret_dec)
        end_d = process_time()
        data = ''.join([i for i in secret])
        data = self.e.decryptor(data.encode())
        self.clean_tmp()
        return end_d - start_d, data

    def clean_tmp(self, path="./tmp"):
        if os.path.exists(path):
            shutil.rmtree(path)
            logging.info("tmp files are cleaned up")

    def encoding_process(self, f_name, input_string=None, encrypt=True, shi=False):
        logging.info("Encoding process started...")
        f_name = self.inputVideoDir + f_name
        self.frame_extraction(f_name, True)
        call(["ffmpeg", "-i", f_name, "-q:a", "0", "-map", "a", "tmp/audio.mp3", "-y"], stdout=open(os.devnull, "w"),
             stderr=STDOUT)
        if encrypt:
            rand = random.randint(85, 99)
            seg = int(rand * len(input_string) / 100)
            logging.info("Input string length: {}".format(len(input_string[:seg])))
            encoding_time, count, full, data_wasted = self.encode_string(self.e.encryptor(input_string[:seg]))
        else:
            encoding_time, seg, count, full, data_wasted = None, None, None, None, None
        call(["ffmpeg", "-i", "tmp/%d.png", "-vcodec", "png", "tmp/video.mov", "-y"], stdout=open(os.devnull, "w"),
             stderr=STDOUT)
        call(["ffmpeg", "-i", "tmp/video.mov", "-i", "tmp/audio.mp3", "-codec", "copy",
              self.outputVideoDir + "video_output.mov", "-y"], stdout=open(os.devnull, "w"), stderr=STDOUT)

        return encoding_time, seg, count, full, data_wasted

    def decoding_process(self, count, decrypt=True, processed_video="video_output.mov", shi=False):
        logging.info("Decoding process started...")
        processed_video = self.outputVideoDir + processed_video
        call(["ffmpeg", "-i", processed_video, "-q:a", "0", "-map", "a", "tmp/audio.mp3", "-y"],
             stdout=open(os.devnull, "w"), stderr=STDOUT)
        if decrypt:
            decode_time, data = self.decode_string(processed_video, count, shi)
        else:
            decode_time = None
            data = None
        call(["ffmpeg", "-i", "tmp/%d.png", "-vcodec", "png", "tmp/video.mov", "-y"],
             stdout=open(os.devnull, "w"),
             stderr=STDOUT)
        call(["ffmpeg", "-i", "tmp/video.mov", "-i", "tmp/audio.mp3", "-codec", "copy",
              self.outputVideoDir + "video_output_shi.mov", "-y"], stdout=open(os.devnull, "w"), stderr=STDOUT)
        return decode_time, data

    def encoding_process_shi_tomashi(self):
        logging.info("Shi-Tomashi process started...")
        f_name = self.inputVideoDir
        self.frame_extraction(f_name)


if __name__ == '__main__':
    e = VideoProcessing()
    e.encoding_process()
