from Crypto.Cipher import AES
import os
import logging

logging.basicConfig(level=logging.INFO)


class AESEncrypt:

    def __init__(self):
        self.key = os.urandom(16)
        self.iv = os.urandom(16)

    def encryptor(self, data):
        logging.info("Encrypting using AES...")
        aes = AES.new(self.key, AES.MODE_CBC, self.iv)
        data = data.encode()
        length = 16 - (len(data) % 16)
        data += bytes([length]) * length
        encrypted_text = aes.encrypt(data)
        logging.info("Encrypting using AES finished.")
        return encrypted_text

    def decryptor(self, cipher):
        logging.info("Decrypting using AES...")
        aes = AES.new(self.key, AES.MODE_CBC, self.iv)
        print(len(cipher))
        length = 16 - (len(cipher) % 16)
        cipher += bytes([length]) * length
        decd = aes.decrypt(cipher)
        decd = decd[:-decd[-1]]
        logging.info("Decrypting using DES finished.")
        try:
            decd = decd.decode()
        except UnicodeDecodeError as e:
            try:
                decd = decd.decode('latin-1')
            except UnicodeDecodeError as e:
                raise e
        return decd
