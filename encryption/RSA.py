from Crypto.Cipher import PKCS1_OAEP
from Crypto.PublicKey import RSA


class RSAEncrypt:

    def __init__(self):
        self.rsa_key = RSA.generate(4096)
        self.private_key = self.rsa_key.export_key('PEM')
        self.public_key = self.rsa_key.publickey().exportKey('PEM')

    def encryptor(self, data):
        rsa_public_key = RSA.importKey(self.public_key)
        rsa_public_key = PKCS1_OAEP.new(rsa_public_key)
        enc_message = rsa_public_key.encrypt(data.encode())
        print("Encrypted string: ", enc_message)

        return enc_message

    def decryptor(self, cipher):
        print(cipher)
        rsa_private_key = RSA.importKey(self.private_key)
        rsa_private_key = PKCS1_OAEP.new(rsa_private_key)
        dec_message = rsa_private_key.decrypt(cipher)
        print("Decrypted string: ", dec_message)
