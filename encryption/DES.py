import pyDes
import logging

hash = "SHA-256"
logging.basicConfig(level=logging.DEBUG)


class DESEncrypt:

    def __init__(self):
        self.__k__ = pyDes.des("DESCRYPT", pyDes.CBC, "\0\0\0\0\0\0\0\0", pad=None, padmode=pyDes.PAD_PKCS5)

    def encryptor(self, data):
        logging.info("Encrypting using DES...")
        d = self.__k__.encrypt(data.encode('latin-1'))
        logging.info("Encrypting using DES finished.")
        return d.decode('latin-1', errors='replace')

    def decryptor(self, cipher):
        """[summary]

        Args:
            cipher ([string]): Cipher text

        Returns:
            [string]: Decrypted text
        """
        logging.info("Decrypting using DES...")
        d = self.__k__.decrypt(cipher.encode('latin-1'))
        logging.info("Decrypting using DES finished.")
        return d
