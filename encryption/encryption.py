import os
from tqdm import tqdm
import pandas as pd
import AES
import random
from time import process_time

hash = "SHA-256"


def load_data():
    data = []
    for root, dirs, files in os.walk('../input/text/test'):
        for fil in files:
            with open(os.path.join(root, fil), 'r', encoding='latin-1') as f:
                data.append(f.read())
    input_text = ' '.join([i for i in data])
    print("Total number of characters", len(input_text))
    return input_text


class Encrypt:

    def __init__(self):
        self.encrypt = AES.AESEncrypt()

    def timer_individual(self, length, data=None):
        if data is None:
            data = load_data()
        segment = int(length * len(data) / 100)
        data = data[:segment]
        start_process = process_time()
        start_encryption = process_time()

        cipher = self.encrypt.encryptor(data)
        end_encryption = process_time()
        start_decryption = process_time()
        dec = self.encrypt.decryptor(cipher)

        end_decryption = process_time()
        end_process = process_time()
        if dec == data:
            success = True
        else:
            success = False
        encrypt_time = end_encryption - start_encryption
        decrpyt_time = end_decryption - start_decryption
        total_process = end_process - start_process
        return total_process, encrypt_time, decrpyt_time, segment, success

    def independent_analysis(self):
        total_times = []
        encrypt_times = []
        decrypt_times = []
        successful = []
        segment = []
        for i in tqdm(range(25)):
            integer = random.randint(15, 20)
            total, encrypt, decrypt, seg, success = self.timer_individual(integer)
            total_times.append(total)
            encrypt_times.append(encrypt)
            decrypt_times.append(decrypt)
            segment.append(seg)
            successful.append(success)
        df = pd.DataFrame({'total': total_times, 'encrypt': encrypt_times, 'decrypt': decrypt_times, 'segment': segment,
                           'successful': successful})
        df['total_per_char'] = df['total'] / df['segment']
        df['encrypt_per_char'] = df['encrypt'] / df['segment']
        df['decrypt_per_char'] = df['decrypt'] / df['segment']
        print(df.head())
        df.to_csv('EncryptionTimes.csv')


if __name__ == "__main__":
    e = Encrypt()
    e.independent_analysis()
