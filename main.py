import logging

from encryption import AES

from time import process_time
import pandas as pd
import os
from tqdm import tqdm
from video import VideoProcessing


class Main:
    def __init__(self):

        self.inputTextDir = "./input/text/test/"
        self.outputTextDir = "./output/text/test/"
        self.inputVideoDir = "./input/video/test/"
        self.outputVideoDir = "./output/video/test/"

    def analysis(self):
        video = VideoProcessing()
        input_string = []
        for root, DIRS, files in os.walk(self.inputTextDir):
            for text in files:
                with open(os.path.join(root, text), 'r', encoding='latin-1') as f:
                    input_string.append(f.read())
        input_string = ''.join([i for i in input_string])
        segment = []
        total_time = []
        total_time_e = []
        total_time_d = []
        data_integrity = []
        data_integrity_pct = []
        for i in tqdm(range(5)):
            start_proc = process_time()
            logging.info("Length of input string: {}".format(len(input_string)))
            encoding, seg, count, full, data_wasted = video.encoding_process('video.mov', input_string=input_string,
                                                                             encrypt=False)
            decoding, data = video.decoding_process(count, decrypt=False)
            end_proc = process_time()
            total_time_e.append(encoding)
            total_time_d.append(decoding)
            total_time.append(end_proc - start_proc)
            segment.append(seg)
            data_integrity.append(full)
            data_integrity_pct.append(data_wasted)
            if data is not None:
                video.write_file(data, self.outputTextDir + "result" + str(i) + ".txt")

            data = pd.DataFrame({'total_time': total_time, 'encoding_time': total_time_e, 'decoding_time': total_time_d,
                                 'characters_hidden': segment, 'data_integrity': data_integrity,
                                 'data_integrity_pct': data_integrity_pct})
            data.to_csv('Stegano_enc_new.csv')
        video.clean_tmp()


if __name__ == "__main__":
    a = Main()
    a.analysis()
